# docker-swarm-gitops-ansible

An example of a GitOps agent for Docker stack manifests (i.e. Docker Swarm) running as an Ansible playbook.

## Prerequisites

* You have at least one config repo containing [Docker stack manifests](https://docs.docker.com/get-started/swarm-deploy)
* You have set your config repos in `group_vars/all.yml#config_repos`.
* You have created Project Access Tokens for your config repos, you have added them as CI/CD variables to this repo, and you have added the names of these CI/CD variables in `group_vars/all.yml#config_repos`.
* You have SSH keys deployed on all target nodes.
    * For every environment/stage, you need the following:
        1. `group_vars/$STAGE.yml` with the stage name
        1. `inventory/$STAGE.yml` with all hostnames or IP addresses
        1. In `.gitlab-ci.yml` a deploy job for your stage
        1. In the CI/CD variables of this repo: The following env vars:
            1. `ANSIBLE_REMOTE_USER_$STAGE` with the username
            1. `ANSIBLE_PRIVATE_KEY_FILE_$STAGE` with the contents of the private key (variable type "File")

## Usage

* Add a Scheduled Pipeline to your repo. Recommended interval: 15 minutes.
* If you need to delete a specific stack in an environment, run the `delete-stack` job manually.
