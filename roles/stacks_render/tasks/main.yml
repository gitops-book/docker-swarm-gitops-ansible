# Render all overlay manifests and save to register

- name: Decrypt dotenv files
  ansible.builtin.include_tasks: sops_decrypt.yml
  loop: "{{ config_repos | dict2items }}"

- name: Find all overlay manifests
  delegate_to: localhost
  loop: "{{ config_repos | dict2items }}"
  ansible.builtin.find:
    paths:
      # Interpolate into filepath string for overlays/stage dir.
      - >-
        {{ item.key
        | regex_replace('^.*$', cloned_repos_directory + '/\g<0>/overlays/' + stage) }}
    patterns:
      - "*.yaml"
      - "*.yml"
    recurse: true
  register: manifest_paths

- &renderOverlays
  name: Render overlay manifests, save to register
  delegate_to: localhost
  # Avoid logging secrets
  no_log: true
  loop: "{{ manifest_paths.results | map(attribute='files') | flatten | map(attribute='path') }}"
  environment: &renderOverlaysEnv
    manifest: "{{ item }}"
  ansible.builtin.shell:
    cmd: |-
      set -o pipefail
      root_dir=${manifest%overlays/*}
      overlay_dir=$(dirname $manifest)
      include_base_manifests=$(\
        grep -E "^\# docker-stack-bases:" "$manifest" \
        | awk -F':' "{print \"-c $root_dir/\" \$NF}" \
        | xargs \
      )
      extra_env=$(\
        find "$overlay_dir" -type f -name '.env' -exec grep -v '^#' {} \; \
        | xargs \
      )
      env $extra_env \
        docker stack config $include_base_manifests -c "$manifest" \
        | sed 's/\$/$$/g'
  register: manifests
  changed_when: manifests is failed
  tags:
    - ci
- <<: *renderOverlays
  environment:
    <<: *renderOverlaysEnv
    # On macOS xargs will not insert replacements exceeding 255 chars unless told otherwise.
    # But on other OSes, this parameter does not exist in xargs and will throw errors.
    xargs_additional_args: -S 100000
  tags:
    - local

# Create rendered manifests on host

- name: Create directories for manifests on host
  # Avoid logging secrets
  no_log: true
  loop: "{{ manifests.results }}"
  ansible.builtin.file:
    group: "{{ user_group }}"
    path: "{{ stack_host_target_directory }}/{{ item.item | dirname }}"
    state: directory
    mode: u=rwx,g=rwx

- name: Create manifest files on host
  # Avoid logging secrets
  no_log: true
  loop: "{{ manifests.results }}"
  ansible.builtin.copy:
    content: "{{ item.stdout }}"
    dest: "{{ stack_host_target_directory }}/{{ item.item }}"
    mode: u=rw,g=rw
  register: manifest_paths_host
