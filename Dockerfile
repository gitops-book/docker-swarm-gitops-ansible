FROM python:3.12.0-alpine

### NOTE: Git is needed for cloning repositories
# renovate: datasource=repology depName=alpine_3_18/git versioning=loose
ENV APK_GIT_VERSION=2.40.1-r0
# renovate: datasource=repology depName=alpine_3_18/openssh-client-default versioning=loose
ENV APK_OPENSSH_VERSION=9.3_p2-r0
### NOTE: age is needed for sops
# renovate: datasource=repology depName=alpine_3_18/age versioning=loose
ENV APK_AGE_VERSION=1.1.1-r7
### NOTE: sops is needed for decrypting dotenv files
# renovate: datasource=repology depName=alpine_edge/sops versioning=loose
ENV APK_SOPS_VERSION=3.7.3-r14
# renovate: datasource=repology depName=alpine_3_18/docker-cli versioning=loose
ENV APK_DOCKER_CLI_VERSION=23.0.6-r6
RUN apk add --no-cache \
    git==$APK_GIT_VERSION \
    openssh-client-default==$APK_OPENSSH_VERSION \
    age==$APK_AGE_VERSION \
    docker-cli==$APK_DOCKER_CLI_VERSION \
  && apk add --no-cache --repository=https://dl-cdn.alpinelinux.org/alpine/edge/testing \
    sops==$APK_SOPS_VERSION

# Add non-root user
ENV USER=ansible
ENV UID=65532
RUN addgroup -g $UID $USER \
    && adduser -G $USER -u $UID -D $USER
USER ansible
 
WORKDIR /home/$USER/app

# Install pipenv & Python dependencies as non-root user
# Add user bin to path so pipenv will be visible
# NOTE: The $HOME variable can be echoed but somehow is empty when
# interpolating during the build.
ENV PATH="/home/$USER/.local/bin:$PATH"
# Avoid buffering Python output
ENV PYTHONUNBUFFERED=1
COPY Pipfile* ./
# renovate: datasource=pypi depName=pipenv versioning=loose
ENV PYPI_PIPENV_VERSION=2023.10.24
RUN pip install --no-cache-dir \
    pipenv==$PYPI_PIPENV_VERSION \
    && pipenv sync --verbose

# Install Ansible collections
COPY requirements.yml ./
# hadolint ignore=DL3013
RUN pipenv run ansible-galaxy install -vvv --role-file requirements.yml

## Set default env vars
# Run playbook for dev env by default
ENV ANSIBLE_INVENTORY=inventory/dev.yml
# Run default playbook including "ci" tagged tasks
ENV ANSIBLE_SKIP_TAGS=local

# Silence warnings about missing healthcheck
HEALTHCHECK CMD exit 0
