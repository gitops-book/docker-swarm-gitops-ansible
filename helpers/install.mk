HELPTEXT_HEADING := Local Installation Targets

.PHONY: install
brew_dependencies = vagrant pyenv
install: ## Install dependencies with Homebrew and pipenv.
	for dep in $(brew_dependencies); do \
		command -v "$$dep" > /dev/null || { brew install "$$dep"; } ; \
	done
	pyenv install "$$(cat .python-version)" --skip-existing
	pip install pipenv
	pipenv sync --dev
	pipenv run ansible-galaxy install -vvv --role-file requirements.yml
