HELPTEXT_HEADING := Local Development Targets:

.PHONY: build.local
build.local: vms.local ## Run the playbook with the local inventory.
	pipenv run vagrant provision --provision-with ansible

.PHONY: vms.local
vms.local: ## Start multiple VMs for local playbook testing. Takes about 20 minutes.
	pipenv run vagrant up

.PHONY: vms.local.stop
local_vms = $(shell vagrant global-status | grep $$(pwd) | awk '{print $$1}')
vms.local.stop: ## Stop the local VMs.
	vagrant halt $(local_vms)

.PHONY: vms.local.restart
vms.local.restart: ## Restart local VMs.
	vagrant reload

.PHONY: clean.vms.local
clean.vms.local: ## Remove local VMs.
	vagrant destroy --force --parallel
